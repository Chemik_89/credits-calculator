package servlets;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


public class TestCreditCalculatorServlet {

    private CreditCalculatorServlet servlet;

    @Before
    public void setupNewServlet() {
        servlet = new CreditCalculatorServlet();
    }

    @Test
    public void given_requestWithoutNecesseryParameters_when_doPost_then_redirectsToRoot_andSetsErrorMessage() throws IOException, ServletException {
        // Given
        HttpServletRequest request = Mockito.mock( HttpServletRequest.class );
        when( request.getParameter( "loan_amount" ) ).thenReturn( null );
        when( request.getParameter( "interest_rate" ) ).thenReturn( null );
        when( request.getParameter( "payments_number" ) ).thenReturn( null );
        when( request.getParameter( "payment_type" ) ).thenReturn( null );

        RequestDispatcher dispatcher = Mockito.mock( RequestDispatcher.class );
        when( request.getRequestDispatcher( any( String.class ) ) ).thenReturn( dispatcher );

        HttpServletResponse response = Mockito.mock( HttpServletResponse.class );

        // When
        servlet.doPost( request, response );

        // Then
        verify( request ).setAttribute( "error_msg", "Brak parametru w zapytaniu" );
        verify( request ).getRequestDispatcher( "/" );
    }

    @Test
    public void given_requestWithEmptyParameters_when_doPost_then_redirectsToRoot_andSetsErrorMessage() throws IOException, ServletException {
        // Given
        HttpServletRequest request = Mockito.mock( HttpServletRequest.class );
        when( request.getParameter( "loan_amount" ) ).thenReturn( "" );
        when( request.getParameter( "interest_rate" ) ).thenReturn( "" );
        when( request.getParameter( "payments_number" ) ).thenReturn( "" );
        when( request.getParameter( "payment_type" ) ).thenReturn( "" );

        RequestDispatcher dispatcher = Mockito.mock( RequestDispatcher.class );
        when( request.getRequestDispatcher( any( String.class ) ) ).thenReturn( dispatcher );

        HttpServletResponse response = Mockito.mock( HttpServletResponse.class );

        // When
        servlet.doPost( request, response );

        // Then
        verify( request ).setAttribute( "error_msg", "Proszę wypełnić wszystkie pola" );
        verify( request ).getRequestDispatcher( "/" );
    }

}