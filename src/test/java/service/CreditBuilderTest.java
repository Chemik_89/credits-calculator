package service;


import domain.Credit;
import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import servlets.EmptyParameterException;
import servlets.NoSuchParameterException;

import javax.servlet.http.HttpServletRequest;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;
import static org.mockito.Mockito.when;


public class CreditBuilderTest {

    private HttpServletRequest request;
    private CreditBuilder builder;

    @Before
    public void instantiateNewRequestMock() {
        request = Mockito.mock( HttpServletRequest.class );
    }

    @Before
    public void instantiateNewCreditBuilder() {
        builder = new CreditBuilder();
    }

    @Test
    public void given_allDataProvided_when_buildCredit_then_returnValidCredit() throws Exception {
        // Given
        String amount = "1200";
        int paymentsNumber = 12;
        String interestPercentage = "5";
        String interestDecimal = "0.05";

        when( request.getParameter( "loan_amount" ) ).thenReturn( amount );
        when( request.getParameter( "payments_number" ) ).thenReturn( Integer.toString( paymentsNumber ) );
        when( request.getParameter( "interest_rate" ) ).thenReturn( interestPercentage );

        // When
        Credit credit = builder.build( request );

        // Then
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat( credit.getAmount() )
                .as( "Credit amount" )
                .isNotNull()
                .isEqualByComparingTo( new BigDecimal( amount ) )
        ;
        softly.assertThat( credit.getPaymentsNumber() )
                .as( "Payments number" )
                .isNotNull()
                .isEqualByComparingTo( paymentsNumber )
        ;
        softly.assertThat( credit.getInterestRate() )
                .as( "Interest rate" )
                .isNotNull()
                .isEqualByComparingTo( new BigDecimal( interestDecimal ) );

        softly.assertAll();
    }

    @Test
    public void given_wrongInputNotParsableToBigDecimal_when_creditBuild_then_throwException() {
        // Given
        String amount = "one hundred";
        int paymentsNumber = 12;
        String interest = "0.05";

        when( request.getParameter( "loan_amount" ) ).thenReturn( amount );
        when( request.getParameter( "payments_number" ) ).thenReturn( Integer.toString( paymentsNumber ) );
        when( request.getParameter( "interest_rate" ) ).thenReturn( interest );

        // When
        Throwable thrown = catchThrowable( () -> builder.build( request ) );

        // Then
        assertThat( thrown ).isInstanceOf( IllegalArgumentException.class );
    }

    @Test
    public void given_wrongInputNotParsableToInt_when_creditBuild_then_throwException() {
        // Given
        String amount = "100";
        String paymentsNumber = "12.5";
        String interest = "0.05";

        when( request.getParameter( "loan_amount" ) ).thenReturn( amount );
        when( request.getParameter( "payments_number" ) ).thenReturn( paymentsNumber );
        when( request.getParameter( "interest_rate" ) ).thenReturn( interest );

        // When
        Throwable thrown = catchThrowable( () -> builder.build( request ) );

        // Then
        assertThat( thrown ).isInstanceOf( IllegalArgumentException.class );
    }

    @Test
    public void given_nullParameter_when_creditBuild_then_throwException() {
        // Given
        String amount = "one hundred";
        String paymentsNumber = "12.0";

        when( request.getParameter( "loan_amount" ) ).thenReturn( amount );
        when( request.getParameter( "payments_number" ) ).thenReturn( paymentsNumber );
        when( request.getParameter( "interest_rate" ) ).thenReturn( null );

        // When
        Throwable thrown = catchThrowable( () -> builder.build( request ) );

        // Then
        assertThat( thrown ).isInstanceOf( NoSuchParameterException.class );
    }

    @Test
    public void given_emptyParameter_when_creditBuild_then_throwException() {
        // Given
        String amount = "100";
        String paymentsNumber = "12";
        String interest = "";

        when( request.getParameter( "loan_amount" ) ).thenReturn( amount );
        when( request.getParameter( "payments_number" ) ).thenReturn( paymentsNumber );
        when( request.getParameter( "interest_rate" ) ).thenReturn( interest );

        // When
        Throwable thrown = catchThrowable( () -> builder.build( request ) );

        // Then
        assertThat( thrown ).isInstanceOf( EmptyParameterException.class );
    }
}