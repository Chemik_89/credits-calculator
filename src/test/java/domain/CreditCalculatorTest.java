package domain;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;


public abstract class CreditCalculatorTest<T extends CreditCalculator> {

    protected abstract T createConcreteInstance();

    @Test
    public void calculator_should_return_payments_number_equal_to_payments_in_credit() throws Exception {
        // Given
        int paymentsNumber = 12;
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getPaymentsNumber() ).thenReturn( paymentsNumber );
        when( credit.getAmount() ).thenReturn( new BigDecimal( "12000" ) );
        when( credit.getInterestRate() ).thenReturn( new BigDecimal( "0.05" ) );

        CreditCalculator calculator = createConcreteInstance();

        // When
        List<CreditPayment> payments =  calculator.calculatePayments( credit );

        // Then
        assertThat( payments ).as( "Number of payments" )
                .isNotNull()
                .hasSize( paymentsNumber )
        ;
    }

    @Test
    public void payments_should_be_sorted() throws Exception {
        // Given
        int paymentsNumber = 12;
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getPaymentsNumber() ).thenReturn( paymentsNumber );
        when( credit.getAmount() ).thenReturn( new BigDecimal( "12000" ) );
        when( credit.getInterestRate() ).thenReturn( new BigDecimal( "0.05" ) );

        CreditCalculator calculator = createConcreteInstance();

        // When
        List<CreditPayment> payments =  calculator.calculatePayments( credit );

        // Then
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat( payments.get( 0 ).getPaymentNumber() )
                .as( "Payment numbers start with 1" )
                .isEqualTo( 1 );
        softly.assertThat( payments ).as( "Payments are sorted" )
                .isNotNull()
                .isSortedAccordingTo( (p, q) -> p.getPaymentNumber() - q.getPaymentNumber() )
        ;
        softly.assertAll();
    }
}