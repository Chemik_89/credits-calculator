package domain.rules;

import domain.Credit;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class PaymentsNumberRuleTest {

    PaymentsNumberRule rule = new PaymentsNumberRule();

    @Test
    public void given_negativePaymentsNumber_thenReturnError() {
        // Given
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getPaymentsNumber() ).thenReturn( -1 );

        // When
        CheckResult result = rule.check( credit );

        // Then
        assertThat( result ).as( "Restult for negative payments number" )
                .isNotNull()
                .isEqualTo( CheckResult.ERROR );
    }

    @Test
    public void given_positivePaymentsNumber_thenReturnOk() {
        // Given
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getPaymentsNumber() ).thenReturn( 10 );

        // When
        CheckResult result = rule.check( credit );

        // Then
        assertThat( result ).as( "Restult for positive payments number" )
                .isNotNull()
                .isEqualTo( CheckResult.OK );
    }
}