package domain.rules;

import domain.Credit;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class AmountRuleTest {

    private AmountRule rule = new AmountRule();

    @Test
    public void given_nullAmount_thenReturnError(){
        // Given
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getAmount() ).thenReturn( null );

        // When
        CheckResult result = rule.check( credit );

        // Then
        assertThat( result ).as( "Restult for null amount" )
                .isNotNull()
                .isEqualTo( CheckResult.ERROR );
    }

    @Test
    public void given_negativeAmount_thenReturnError() {
        // Given
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getAmount() ).thenReturn( new BigDecimal( "-1" ) );

        // When
        CheckResult result = rule.check( credit );

        // Then
        assertThat( result ).as( "Restult for negative amount" )
                .isNotNull()
                .isEqualTo( CheckResult.ERROR );
    }

    @Test
    public void given_positiveAmount_thenReturnOk() {
        // Given
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getAmount() ).thenReturn( new BigDecimal( "00.01" ) );

        // When
        CheckResult result = rule.check( credit );

        // Then
        assertThat( result ).as( "Restult for positive amount" )
                .isNotNull()
                .isEqualTo( CheckResult.OK );
    }

}