package domain.rules;


import domain.Credit;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class InterestRateRuleTest {

    InterestRateRule rule = new InterestRateRule();

    @Test
    public void given_nullInterestRate_thenReturnError(){
        // Given
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getInterestRate() ).thenReturn( null );

        // When
        CheckResult result = rule.check( credit );

        // Then
        assertThat( result ).as( "Restult for null interest rate" )
                .isNotNull()
                .isEqualTo( CheckResult.ERROR );
    }

    @Test
    public void given_negativeOrBiggerThanOneInterestRate_thenReturnError() {
        // Given
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getInterestRate() )
                .thenReturn( new BigDecimal( "-0.01" ) )
                .thenReturn( new BigDecimal( "1.01" ) );

        // When
        CheckResult negative = rule.check( credit );
        CheckResult tooBig = rule.check( credit );

        // Then
        SoftAssertions softly = new SoftAssertions();

        softly.assertThat( negative ).as( "Restult for negative interest rate" )
                .isNotNull()
                .isEqualTo( CheckResult.ERROR );

        softly.assertThat( tooBig ).as( "Restult for interest rate bigger than 1" )
                .isNotNull()
                .isEqualTo( CheckResult.ERROR );

        softly.assertAll();
    }

    @Test
    public void given_correctInterestRate_thenReturnOk() {
        // Given
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getInterestRate() ).thenReturn( new BigDecimal( "0.15" ) );

        // When
        CheckResult result = rule.check( credit );

        // Then
        assertThat( result ).as( "Restult for correct interest rate" )
                .isNotNull()
                .isEqualTo( CheckResult.OK );
    }
}