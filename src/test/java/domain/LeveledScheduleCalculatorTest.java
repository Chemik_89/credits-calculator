package domain;


import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class LeveledScheduleCalculatorTest extends CreditCalculatorTest< LeveledScheduleCalculator > {

    protected LeveledScheduleCalculator createConcreteInstance() {
        return new LeveledScheduleCalculator();
    }

    @Test
    public void all_payments_has_equal_total() throws Exception {
        // Given
        int paymentsNumber = 240;
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getPaymentsNumber() ).thenReturn( paymentsNumber );
        when( credit.getAmount() ).thenReturn( new BigDecimal( "200000" ) );
        when( credit.getInterestRate() ).thenReturn( new BigDecimal( "0.05" ) );

        CreditCalculator calculator = new LeveledScheduleCalculator();

        // When
        List<CreditPayment> payments =  calculator.calculatePayments( credit );

        // Then
        assertThat( payments ).as( "Principal of payments" )
                .isNotNull()
                .extracting( CreditPayment::getTotal )
                .containsOnly( new BigDecimal( "1319.91" ) )
        ;
    }
}