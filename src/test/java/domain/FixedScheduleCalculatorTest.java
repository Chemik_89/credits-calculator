package domain;

import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class FixedScheduleCalculatorTest  extends CreditCalculatorTest<FixedScheduleCalculator> {
    protected FixedScheduleCalculator createConcreteInstance() {
        return new FixedScheduleCalculator();
    }


    @Test
    public void all_rates_has_even_principal() throws Exception {
        // Given
        int paymentsNumber = 12;
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getPaymentsNumber() ).thenReturn( paymentsNumber );
        when( credit.getAmount() ).thenReturn( new BigDecimal( "12000" ) );
        when( credit.getInterestRate() ).thenReturn( new BigDecimal( "0.05" ) );

        CreditCalculator calculator = new FixedScheduleCalculator();

        // When
        List<CreditPayment> payments =  calculator.calculatePayments( credit );

        // Then
        assertThat( payments ).as( "Principal of payments" )
                .isNotNull()
                .extracting( CreditPayment::getPrincipal )
                .containsOnly( new BigDecimal( 1000 ) )
        ;
    }

    @Test
    public void test_interest_of_payments() throws Exception {
        // Given
        int paymentsNumber = 12;
        Credit credit = Mockito.mock( Credit.class );
        when( credit.getPaymentsNumber() ).thenReturn( paymentsNumber );
        when( credit.getAmount() ).thenReturn( new BigDecimal( "12000" ) );
        when( credit.getInterestRate() ).thenReturn( new BigDecimal( "0.05" ) );

        CreditCalculator calculator = new FixedScheduleCalculator();

        // When
        List<CreditPayment> payments =  calculator.calculatePayments( credit );

        // Then
        assertThat( payments ).as( "Intrest of payments" )
                .isNotNull()
                .extracting( CreditPayment::getInterest )
                .containsOnly( new BigDecimal( "50.00" ),
                        new BigDecimal( "45.83" ),
                        new BigDecimal( "41.67" ),
                        new BigDecimal( "37.50" ),
                        new BigDecimal( "33.33" ),
                        new BigDecimal( "29.17" ),
                        new BigDecimal( "25.00" ),
                        new BigDecimal( "20.83" ),
                        new BigDecimal( "16.67" ),
                        new BigDecimal( "12.50" ),
                        new BigDecimal( "8.33" ),
                        new BigDecimal( "4.17" )
                )
        ;
    }
}