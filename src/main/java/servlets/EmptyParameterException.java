package servlets;

public class EmptyParameterException extends RuntimeException {
    public EmptyParameterException() {
    }

    public EmptyParameterException( String message ) {
        super( message );
    }

    public EmptyParameterException( String message, Throwable cause ) {
        super( message, cause );
    }

    public EmptyParameterException( Throwable cause ) {
        super( cause );
    }
}
