package servlets;

public class NoSuchParameterException extends RuntimeException {
    public NoSuchParameterException() {
    }

    public NoSuchParameterException( String message ) {
        super( message );
    }

    public NoSuchParameterException( String message, Throwable cause ) {
        super( message, cause );
    }

    public NoSuchParameterException( Throwable cause ) {
        super( cause );
    }
}
