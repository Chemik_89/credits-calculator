package servlets;

public class UnknownParameterValueException extends RuntimeException {
    public UnknownParameterValueException() {
    }

    public UnknownParameterValueException( String message ) {
        super( message );
    }

    public UnknownParameterValueException( String message, Throwable cause ) {
        super( message, cause );
    }

    public UnknownParameterValueException( Throwable cause ) {
        super( cause );
    }
}
