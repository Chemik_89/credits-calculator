package servlets;

import domain.*;
import service.CreditBuilder;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet( "/calculate_credit" )
public class CreditCalculatorServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        String forward = "/";
        String errorMsg = null;

        try {
            calculateCreditPayments( request );
            forward = "/WEB-INF/credit_payments.jsp";
        }
        catch ( EmptyParameterException e ) {
            errorMsg = "Proszę wypełnić wszystkie pola";
        }
        catch ( UnknownParameterValueException e ){
            errorMsg = "Nieznana wartość parametru";
        }
        catch ( NoSuchParameterException e ) {
            errorMsg = "Brak parametru w zapytaniu";
        }
        catch ( NumberFormatException e ) {
            errorMsg = "Nieprawidłowy format danych";
        }
        catch ( IllegalArgumentException e ) {
            errorMsg = "Podano nieprawidłową wartość";
        }
        finally {
            if ( errorMsg != null ) {
                request.setAttribute( "error_msg", errorMsg );
            }
            request.getRequestDispatcher( forward ).forward( request, response );
        }
    }

    private void calculateCreditPayments( HttpServletRequest request ) {
        CreditBuilder builder = new CreditBuilder();
        Credit credit = builder.build( request );

        CreditCalculator calculator = selectCalculator( request );
        List< CreditPayment > list = calculator.calculatePayments( credit );

        request.setAttribute( "payments_list", list );
    }

    private CreditCalculator selectCalculator( HttpServletRequest request ) {
        String scheduleType = request.getParameter( "payment_type" );
        if ( scheduleType == null ) {
            throw new NoSuchParameterException();
        }
        if ( scheduleType.equals( "" ) ) {
            throw new EmptyParameterException(  );
        }
        else {
            CreditCalculator calculator;
            switch ( scheduleType ) {
                case "fixed":
                    calculator = new FixedScheduleCalculator();
                    break;
                case "leveled":
                    calculator = new LeveledScheduleCalculator();
                    break;
                default:
                    throw new UnknownParameterValueException();
            }
            return calculator;
        }
    }
}