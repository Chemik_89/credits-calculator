package service;

import domain.Credit;
import domain.rules.*;
import servlets.EmptyParameterException;
import servlets.NoSuchParameterException;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;


public class CreditBuilder {

    private CreditChecker checker = new CreditChecker();

    public Credit build( HttpServletRequest request ) {

        String loanAmount = request.getParameter( "loan_amount" );
        String interestRate = request.getParameter( "interest_rate" );
        String paymentsNumber = request.getParameter( "payments_number" );

        test( loanAmount );
        test( interestRate );
        test( paymentsNumber );

        BigDecimal loanAmountBD = new BigDecimal( loanAmount );
        BigDecimal interestRateBD = new BigDecimal( interestRate )
                .divide( new BigDecimal( "100") , 2, BigDecimal.ROUND_HALF_UP );
        int paymentsNumberInt = Integer.parseInt( paymentsNumber );

        Credit credit = new Credit();
        credit.setAmount( loanAmountBD );
        credit.setInterestRate( interestRateBD );
        credit.setPaymentsNumber( paymentsNumberInt );

        if ( checker.check( credit ) != CheckResult.OK ) {
            throw new IllegalArgumentException(  );
        }

        return credit;
    }

    private void test( String s ) {
        if (s == null) {
            throw new NoSuchParameterException(  );
        }
        if (s.equals( "" )) {
            throw new EmptyParameterException(  );
        }
    }
}