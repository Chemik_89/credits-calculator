package domain.rules;

public interface Rule< T > {
    CheckResult check( T entity );
}