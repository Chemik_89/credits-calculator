package domain.rules;

import domain.Credit;

import java.util.ArrayList;
import java.util.List;

public class CreditChecker implements Rule< Credit > {

    private List< Rule<Credit> > rulesList = new ArrayList<>(  );

    public CreditChecker() {
        rulesList.add( new AmountRule() );
        rulesList.add( new InterestRateRule() );
        rulesList.add( new PaymentsNumberRule() );
    }

    public CheckResult check( Credit credit ) {
        for ( Rule< Credit > rule : rulesList ) {
            if ( rule.check( credit ) != CheckResult.OK ) {
                return CheckResult.ERROR;
            }
        }
        return CheckResult.OK;
    }
}