package domain.rules;

import domain.Credit;

import java.math.BigDecimal;

class InterestRateRule implements Rule< Credit > {

    public CheckResult check( Credit credit) {
        BigDecimal interestRate = credit.getInterestRate();

        if ( interestRate != null
                && isGraterThanOrEqualToZero( interestRate )
                && isLessThanOrEqualToOne( interestRate )
                ) {
            return CheckResult.OK;
        }
        else {
            return CheckResult.ERROR;
        }
    }

    private boolean isGraterThanOrEqualToZero( BigDecimal interestRate ) {
        return interestRate.compareTo( BigDecimal.ZERO ) >= 0;
    }

    private boolean isLessThanOrEqualToOne( BigDecimal interestRate ) {
        return interestRate.compareTo( BigDecimal.ONE ) <= 0;
    }
}