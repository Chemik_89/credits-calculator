package domain.rules;

import domain.Credit;

import java.math.BigDecimal;

class AmountRule implements Rule< Credit > {
    public CheckResult check( Credit credit) {
        if ( credit.getAmount() == null ) {
            return CheckResult.ERROR;
        }
        if ( credit.getAmount().compareTo( BigDecimal.ZERO ) != 1  ) {
            return CheckResult.ERROR;
        }

        return CheckResult.OK;
    }
}
