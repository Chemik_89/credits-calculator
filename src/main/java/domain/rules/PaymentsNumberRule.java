package domain.rules;

import domain.Credit;

class PaymentsNumberRule implements Rule< Credit > {

    public CheckResult check( Credit credit) {
        if ( credit.getPaymentsNumber() < 1 ) {
            return CheckResult.ERROR;
        }
        return CheckResult.OK;
    }
}