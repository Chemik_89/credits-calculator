package domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FixedScheduleCalculator implements CreditCalculator {
    public List< CreditPayment > calculatePayments( Credit credit ) {

        List<CreditPayment> paymentsList = new ArrayList<>(  );

        int paymentsNumber = credit.getPaymentsNumber();
        BigDecimal principal = credit.getAmount().divide( new BigDecimal( paymentsNumber ), 2 );
        BigDecimal interestRate = credit.getInterestRate();
        BigDecimal amountLeft = credit.getAmount();
        BigDecimal paymentsPerYear = new BigDecimal( 12 );

        for (int i = 1; i <= paymentsNumber; i++) {
            BigDecimal interest = amountLeft.multiply( interestRate )
                    .divide( paymentsPerYear, 2, BigDecimal.ROUND_HALF_UP )
                    ;
            amountLeft = amountLeft.subtract( principal );

            paymentsList.add( new CreditPayment( i, principal, interest) );
        }
        return paymentsList;
    }
}