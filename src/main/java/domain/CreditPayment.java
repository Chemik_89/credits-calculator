package domain;

import java.math.BigDecimal;

public class CreditPayment {
    private int paymentNumber;
    private BigDecimal principal;
    private BigDecimal interest;

    public CreditPayment() {
    }

    public CreditPayment( int paymentNumber, BigDecimal principal, BigDecimal interest ) {
        this.paymentNumber = paymentNumber;
        this.principal = principal;
        this.interest = interest;
    }

    public int getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber( int paymentNumber ) {
        this.paymentNumber = paymentNumber;
    }

    public BigDecimal getPrincipal() {
        return principal;
    }

    public void setPrincipal( BigDecimal principal ) {
        this.principal = principal;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest( BigDecimal interest ) {
        this.interest = interest;
    }

    public BigDecimal getTotal() {
        return principal.add( interest );
    }
}
