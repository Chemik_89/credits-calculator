package domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class LeveledScheduleCalculator implements CreditCalculator {

    public List< CreditPayment > calculatePayments( Credit credit ) {

        List<CreditPayment> paymentsList = new ArrayList<>(  );

        BigDecimal totalPayment = calculateTotalPaymentValue( credit );

        BigDecimal paymentsPerYear = new BigDecimal( 12 );
        BigDecimal interestRatePerPayment = credit.getInterestRate().divide( paymentsPerYear, 6, BigDecimal.ROUND_HALF_UP );
        BigDecimal amountLeft = credit.getAmount();

        int paymentsNumber = credit.getPaymentsNumber();
        for (int i = 1; i <= paymentsNumber; i++) {
            BigDecimal interest = amountLeft.multiply( interestRatePerPayment ).setScale( 2, BigDecimal.ROUND_HALF_UP );
            BigDecimal principal = totalPayment.subtract( interest ).setScale( 2, BigDecimal.ROUND_HALF_UP );
            amountLeft = amountLeft.subtract( principal );

            paymentsList.add(  new CreditPayment( i, principal, interest) );
        }
        return paymentsList;
    }

    private BigDecimal calculateTotalPaymentValue( Credit credit ) {
        int paymentsNumber = credit.getPaymentsNumber();
        double paymentsPerYear = 12;
        double interestRate = credit.getInterestRate().doubleValue();
        double interestRatePerPayment = interestRate / paymentsPerYear;
        double q = 1 + interestRatePerPayment;
        double factor = ( (q - 1) * Math.pow( q, paymentsNumber ) ) / ( Math.pow( q, paymentsNumber ) - 1 );

        return credit.getAmount().multiply( new BigDecimal( factor ) );
    }
}