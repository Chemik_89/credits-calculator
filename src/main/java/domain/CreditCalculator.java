package domain;

import java.util.List;

public interface CreditCalculator {

    List< CreditPayment > calculatePayments( Credit credit );
}