package domain;

import java.math.BigDecimal;


public class Credit {

    private BigDecimal amount;
    private BigDecimal interestRate;
    private int paymentsNumber;


    public Credit() {
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount( BigDecimal amount ) {
        this.amount = amount;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate( BigDecimal interestRate ) {
        this.interestRate = interestRate;
    }

    public int getPaymentsNumber() {
        return paymentsNumber;
    }

    public void setPaymentsNumber( int paymentsNumber ) {
        this.paymentsNumber = paymentsNumber;
    }
}
