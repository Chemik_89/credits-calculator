<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<title>Kalkulator kredytowy</title>
</head>
<body>
    <div class="container">
        <c:if test="${error_msg != null}">
            <div class="alert alert-danger" role="alert">
                <p>
                    <c:out value="${error_msg}" />
                </p>
            </div>
        </c:if>
        <form class="form-horizontal" role="form" action="/calculate_credit" method="post">
            <div class="form-group">
                <label class="control-label col-sm-2" for="loan_amount">Kwota kredytu:</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="loan_amount" name="loan_amount"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="payments_number">Ilość rat:</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="payments_number" name="payments_number"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="interest_rate">Oprocentowanie:</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="interest_rate" name="interest_rate"/>
                </div>
            </div>
            <%--<div class="form-group">--%>
                <%--<label class="control-label col-sm-2" for="fixed_charge">Opłata stała:</label>--%>
                <%--<div class="col-sm-10">--%>
                    <%--<input class="form-control" type="text" id="fixed_charge" name="fixed_charge"/>--%>

                <%--</div>--%>
            <%--</div>--%>
            <div class="form-group">
                <label class="control-label col-sm-2">Rodzaj raty: </label>
                <div class="col-sm-10">
                    <label class="radio-inline"><input type="radio" id="payment_type_0" name="payment_type" value="fixed" checked/>malejąca</label>
                    <label class="radio-inline"><input type="radio" id="payment_type_1" name="payment_type" value="leveled"/>stała</label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input class="btn btn-default" type="submit" value="Wyslij"/>
                </div>
            </div>
        </form>
    </div>
</body>
</html>