<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Kalkulator kredytowy - harmonogram spłay</title>
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="sm">
            <table class="table table-striped table-condensed table-bordered">
                <caption>Harmonogram spłaty kredytu</caption>
                <thead>
                    <tr>
                        <th class="col-md-1">Rata Nr</th>
                        <th class="col-md-2">Kwota Kapitału</th>
                        <th class="col-md-2">Kwota odsetek</th>
                        <th class="col-md-2">Kwota całkowita</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="payment" items="${payments_list}">
                        <tr>
                            <td><c:out value="${payment.getPaymentNumber()}"/></td>
                            <td><c:out value="${payment.getPrincipal()}"/></td>
                            <td><c:out value="${payment.getInterest()}"/></td>
                            <td><c:out value="${payment.getTotal()}" /></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>